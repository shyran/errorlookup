#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

// Menu Id
#define IDM_MAIN 100
#define IDM_LISTVIEW 101
#define IDM_MODULES 102

// Dialogs
#define IDD_MAIN 101
#define IDD_SETTINGS 102
#define IDD_SETTINGS_1 103
#define IDD_SETTINGS_2 104
#define IDD_SETTINGS_3 105

// Main Dlg
#define IDC_CODE 100
#define IDC_CODE_UD 101
#define IDC_CODE10 102
#define IDC_CODE10_CTL 103
#define IDC_CODE16 104
#define IDC_CODE16_CTL 105
#define IDC_SEVERITY 106
#define IDC_SEVERITY_CTL 107
#define IDC_FACILITY 108
#define IDC_FACILITY_CTL 109
#define IDC_LISTVIEW 110
#define IDC_DESCRIPTION 111

// Settings Dlg
#define IDC_NAV 1000
#define IDC_APPLY 1001
#define IDC_CLOSE 1002

#define IDC_TITLE_1 1003
#define IDC_TITLE_2 1004
#define IDC_TITLE_3 1005

#define IDC_ALWAYSONTOP_CHK 100
#define IDC_INSERTBUFFER_CHK 101
#define IDC_CHECKUPDATES_CHK 102

#define IDC_LANGUAGE_HINT 103
#define IDC_LANGUAGE 104

#define IDC_MODULES 105

#define IDC_MODULE_INTERNAL_CPP 106
#define IDC_MODULE_INTERNAL_DX 107

// Main Menu
#define IDM_SETTINGS 1000
#define IDM_WEBSITE 1001
#define IDM_CHECKUPDATES 1002
#define IDM_ABOUT 1003
#define IDM_EXIT 1004

// Listview Menu
#define IDM_COPY 2000

// Modules Menu
#define IDM_ADD 3000
#define IDM_DELETE 3001

// Strings
#define IDS_LCID 1000

#define IDS_UPDATE_NO 1001
#define IDS_UPDATE_YES 1002

#define IDS_FILE 1003
#define IDS_SETTINGS 1004
#define IDS_EXIT 1005
#define IDS_HELP 1006
#define IDS_WEBSITE 1007
#define IDS_CHECKUPDATES 1008
#define IDS_ABOUT 1009

#define IDS_COPY 1010

#define IDS_ADD 1011
#define IDS_DELETE 1012

#define IDS_CODE10 1013
#define IDS_CODE16 1014
#define IDS_SEVERITY 1015
#define IDS_FACILITY 1016

#define IDS_APPLY 1017
#define IDS_CLOSE 1018

#define IDS_TITLE_1 1019
#define IDS_TITLE_2 1020
#define IDS_TITLE_3 1021

#define IDS_ALWAYSONTOP_CHK 1022
#define IDS_INSERTBUFFER_CHK 1023
#define IDS_CHECKUPDATES_CHK 1024

#define IDS_LANGUAGE_HINT 1025

#define IDS_MODULE_INTERNAL_CPP 1026
#define IDS_MODULE_INTERNAL_DX 1027

#define IDS_COLUMN_1 1028
#define IDS_COLUMN_2 1029

#define IDS_GROUP_1 1030
#define IDS_GROUP_2 1031

#define IDS_SETTINGS_1 1032
#define IDS_SETTINGS_2 1033
#define IDS_SETTINGS_3 1034

// Icons
#define IDI_MAIN 100

#endif // __RESOURCE_H__
